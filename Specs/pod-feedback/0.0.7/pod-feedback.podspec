Pod::Spec.new do |s|
  s.name              = "pod-feedback"
  s.version           = "0.0.7"
  s.summary           = "Feedback page for Metova iOS projects"
  s.homepage          = "http://metova.com"
  s.license           = 'MIT'
  s.authors           = { "Skye McCaskey" => "skye.mccaskey@metova.com"}
  s.source            = { :git => "https://bitbucket.org/metova/pod-feedback.git", :tag => s.version.to_s }
  s.platform          = :ios, '7.0'
  s.requires_arc      = true
  s.source_files      = 'MetovaFeedback/*.{h,m,swift,storyboard}'
  s.resources         = "MetovaFeedback/Resources/*.{png}"
  s.dependency        'AFNetworking'
end
